export const Menu = () => {
    const menu = ['home', 'projects', 'measurement', 'team', 'reviews', 'contacts']
    const menuArray = menu.map( item => {
        if (item === 'home') {
            return `
                <li class="menu__item">
                    <a href="/">${ item }</a>
                </li>
            `
        } else {
            return `
                <li class="menu__item">${ item }</li>
            `
        }
    }).join('')
    return menuArray
}