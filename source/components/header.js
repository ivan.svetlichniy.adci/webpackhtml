import { Menu } from "./menu"

export const Header = () => {
    const root = document.querySelector('.root')
    const header = document.createElement('header')

    header.innerHTML = `
        <div class="container-narrow">
            <header class="header">
                <div class="logo">
                    <a href="/">IC "Repair Design Project"</a>
                </div>
                <nav class="menu">
                    <ul class="menu__list">
                        ${ Menu() }
                    </ul>
                </nav>
                <div class="phone-number">
                    <a href="tel:+79287683239">+7 (928) 768-32-39</a>
                </div>
                <button class="button button--menu">request a call</button>
            </header>
        </div>    
    `
    root.append(header)
}